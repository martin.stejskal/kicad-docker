FROM ubuntu:20.04

USER root
RUN apt update
RUN apt install -y software-properties-common
RUN add-apt-repository --yes ppa:kicad/kicad-5.1-releases
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y --install-recommends kicad
RUN apt install -y sudo

# Arguments allow dynamically set same user ID ;)
# Also it should be defined after base is installed to avoid re-downloading
ARG UNAME=kicad
ARG UID=1000
ARG GID=100

RUN groupadd -g $GID -o kicad_users && \
    useradd -m -g $GID -o -s /bin/bash -u $UID $UNAME && \
    echo "$UNAME ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$UNAME && \
    chmod 0440 /etc/sudoers.d/$UNAME

USER $UNAME
RUN  mkdir -p /home/$UNAME/host
ENV HOME /home/$UNAME
CMD kicad
