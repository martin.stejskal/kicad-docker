# About
 * If you know what is [Docker](https://www.docker.com/), then you can run
   [KiCad](http://kicad-pcb.org/) on your system without need to install
   dependencies, which may break your system.
 * You can find here sources to Docker image with latest stable KiCad
   preinstalled
 * Many thanks to
   [Fabio Rehm's Blog
](http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/)


# Run
 * Simply run `kicad_docker.sh`. Script will build image (needed only for first
   time) and run KiCad for you.
 * Some paramters need to be passed to container, but BFU should not care
   about it. Else you're free to look into script ;)

# "Uninstall" KiCad
 * Technically speaking, you need to stop container (close KiCad application),
   delete container and then detele image. Simply copy/paste following to
   console:

```bash
docker stop KiCad
docker rm KiCad
docker rmi kicad_img
```

# Update KiCad
 * Make sure that KiCad is running.
 * Run following command from host:

```bash
docker exec KiCad bash -c 'apt update && apt upgrade -y'
```

 * Close KiCad application and run again


# Advanced
 * In 99.9% you do not need to read this. But if you want to modify something,
   this might be good start.

## Building image

```bash
docker build -t kicad_img .
```

## Run one shot instance of KiCad
 * If something does not work for you, this might be way to figure out.

```bash
docker run -ti --rm \
        -e DISPLAY=$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v $HOME/.Xauthority:/home/kicad/.Xauthority \
        --net=host \
        -v /:/home/kicad/host \
        --name KiCad kicad_img
```
